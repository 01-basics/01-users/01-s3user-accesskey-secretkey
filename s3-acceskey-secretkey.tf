resource "aws_iam_access_key" "S3AccessKey" {
  user    = aws_iam_user.S3AccessKey.name
  pgp_key = "keybase:some_person_that_exists"
}

resource "aws_iam_user" "S3AccessKey" {
  name = "s3-access-key"
  path = "/system/"
}

data "aws_iam_policy_document" "S3AccessKeyPolicy" {
  statement {
    effect    = "Allow"
    actions   = ["S3:*"]
    resources = ["*"]
  }
}

resource "aws_iam_user_policy" "S3AccessKeyUserPolicy" {
  name   = "s3-access-key-user-policy"
  user   = aws_iam_user.s3-access-key.name
  policy = data.aws_iam_policy_document.lb_ro.json
}

